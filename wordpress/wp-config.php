<?php
/**
 * The base configuration for WordPress
 *
 * The wp-config.php creation script uses this file during the
 * installation. You don't have to use the web site, you can
 * copy this file to "wp-config.php" and fill in the values.
 *
 * This file contains the following configurations:
 *
 * * MySQL settings
 * * Secret keys
 * * Database table prefix
 * * ABSPATH
 *
 * @link https://codex.wordpress.org/Editing_wp-config.php
 *
 * @package WordPress
 */

// ** MySQL settings - You can get this info from your web host ** //
/** The name of the database for WordPress */
define( 'DB_NAME', 'wptest1-test' );

/** MySQL database username */
define( 'DB_USER', 'wordpress' );

/** MySQL database password */
define( 'DB_PASSWORD', 'password' );

/** MySQL hostname */
define( 'DB_HOST', 'mysql' );

/** Database Charset to use in creating database tables. */
define( 'DB_CHARSET', 'utf8' );

/** The Database Collate type. Don't change this if in doubt. */
define( 'DB_COLLATE', '' );

/**
 * Authentication Unique Keys and Salts.
 *
 * Change these to different unique phrases!
 * You can generate these using the {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org secret-key service}
 * You can change these at any point in time to invalidate all existing cookies. This will force all users to have to log in again.
 *
 * @since 2.6.0
 */
define( 'AUTH_KEY',          '(SctUGnU8Vf?SgzZ^3$ED4Kh}n;6Q|ex?Cmg/QK}JU4OtxGg`B.:UJQPrgYL:M.*' );
define( 'SECURE_AUTH_KEY',   'h,BWR#RSkGyp^-AwH@_[%3nWoDzz1]7/;kCzr6 3/=9S,w^yj S6J>-~m7B#W_ep' );
define( 'LOGGED_IN_KEY',     'Yp1yIF.( HNP.w-1oA=?euB;xd/GHIUP=cz$;(4<T9AY=m:< WwkcYTT9a42@Rg&' );
define( 'NONCE_KEY',         '3WbyNN0YwHN+^]@@B^3+S<=.SE3C5+}LQ}+)mmDO #P<Pp)&L~_Ce:U#D0ZB_V2|' );
define( 'AUTH_SALT',         'BD9LB;ZVJ`MLF7)ZY*%>76Dzxp||eM<@o4LO7:CSm!:9FK5*N@MI0Jv<U?o0/)7E' );
define( 'SECURE_AUTH_SALT',  ']rlk((et=k[X& :8R8rvTY&j+G#y$bLS-.bKc!0uG<Mt7{eO~;0^kDhQ}{BNr>IW' );
define( 'LOGGED_IN_SALT',    '^alG#9T=/lm|lA73PngOF.GP%)Dz]$YE*a>w4lm`y!<XjTpUT 2`qK%jtCwc[%4:' );
define( 'NONCE_SALT',        'oew[;Pl12gr3[:{R-ZVZzwKUbDClj_) qVy|Bj}X`Smju?qsBw)eh@]WNcL^~Mo~' );
define( 'WP_CACHE_KEY_SALT', '@-A#UwkEx9&R3Dq#CNtVll>FTZ16mf|WXx4X|6Ek.J1)=n_k_vW}8aG?:_L*GoYV' );

/**
 * WordPress Database Table prefix.
 *
 * You can have multiple installations in one database if you give each
 * a unique prefix. Only numbers, letters, and underscores please!
 */
$table_prefix = 'wp_';




/* That's all, stop editing! Happy publishing. */

/** Absolute path to the WordPress directory. */
if ( ! defined( 'ABSPATH' ) ) {
	define( 'ABSPATH', dirname( __FILE__ ) . '/' );
}

/** Sets up WordPress vars and included files. */
require_once ABSPATH . 'wp-settings.php';
